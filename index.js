const express = require("express")
const mongoose = require("mongoose")
const dotenv = require("dotenv")

dotenv.config()

const app = express()
const port = 3001

// MongoDB Connection
mongoose.connect(`mongodb+srv://lits:${process.env.MONGODB_PASSWORD}@cluster0.kuy6pka.mongodb.net/?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection

db.on("error", () => console.error("Connection error."))
db.on("open", () => console.log("Connected to MongoDB!"))
// MongoDB Connection END

app.use(express.json())
app.use(express.urlencoded({extended: true}))

// MongoDB Schemas
const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: 'Pending'
    }
})
// MongoDB Schemas END

// MongoDB Model
const Task = mongoose.model('Task', taskSchema)
// MongoDB Model END

// Routes
app.post('/tasks', (req, res) => {
    Task.findOne({name: req.body.name}, (error, result) => {
        if(result != null && result.name == req.body.name){
            return res.send('Duplicate task found!')
        }

        let newTask = new Task({
            name: req.body.name
        })

        newTask.save((error, savedTask) => {
            if(error){
                return console.error(error)
            }
            else {
                return res.status(200).send('New task created!')
            }
        })
    })
})

app.get('/tasks', (req, res) => {
    Task.find({}, (error, result) => {
        if(error) {
            return console.log(error)
        }

        return res.status(200).json({
            data: result
        })
    })
})
// Routes END

// Activity
// 1. Create a User schema
const userSchema = new mongoose.Schema({
    name: String,
    password: String,
    status: {
        type: String,
        default: 'Pending'
    }
})

// 2. Create a User model
const User = mongoose.model('User', userSchema)

// 3. Create a POST route that will access the /signup route that will create a user.
app.post('/signup', (req, res) => {
    User.findOne({name: req.body.name}, (error, result) => {
        if(result != null && result.name == req.body.name){
            return res.send('Duplicate user found!')
        }

        let newUser = new User({
            name: req.body.name,
            password: req.body.password,
            status: req.body.status
        })

        newUser.save((error, savedUser) => {
            if(error){
                return console.error(error)
            }
            else {
                return res.status(200).send('New user created!')
            }
        })
    })
})

// 4. Process a POST request at the /signup route using postman to register a user.


app.listen(port, () => console.log(`Server running at localhost:${port}`))